package main

import (
	"fmt"
	"os"
	"strconv"
	"text/tabwriter"
)

var (
	BASEPRIO_CORE_RZ   = 4096
	BASEPRIO_CORE_MZ   = 8192
	BASEPRIO_ACCESS_RZ = 28672
	BASEPRIO_ACCESS_MZ = 32768
	BASEPRIO_TOR_RH_RZ = 20480
	BASEPRIO_TOR_RH_MZ = 24576
)

func Print_usage() {
	fmt.Println("This is the usage page")
}

func CalcPrios(vlans []int) {
	w := new(tabwriter.Writer)
	w.Init(os.Stdout, 8, 8, 0, '\t', 0)
	defer w.Flush()

	fmt.Fprintf(w, "\n %s\t%s\t%s\t%s\t%s\t%s\t%s\t", "VLAN", "PRIO Core RZ", "PRIO Core MZ", "PRIO ToR/RH RZ", "PRIO ToR/RH MZ", "PRIO Access RZ", "PRIO Access MZ")
	fmt.Fprintf(w, "\n %s\t%s\t%s\t%s\t%s\t%s\t%s\t", "----", "------------", "------------", "--------------", "--------------", "--------------", "--------------")

	for _, vlan := range vlans {
		prioCoreRz := BASEPRIO_CORE_RZ + vlan
		prioCoreMz := BASEPRIO_CORE_MZ + vlan
		prioAccessRZ := BASEPRIO_ACCESS_RZ + vlan
		prioAccessMZ := BASEPRIO_ACCESS_MZ + vlan
		prioTorRhRz := BASEPRIO_TOR_RH_RZ + vlan
		prioTorRhMz := BASEPRIO_TOR_RH_MZ + vlan
		fmt.Fprintf(w, "\n %d\t%d\t%d\t%d\t%d\t%d\t%d\t", vlan, prioCoreRz, prioCoreMz, prioTorRhRz, prioTorRhMz, prioAccessRZ, prioAccessMZ)
	}
}
func main() {

	argsWithProg := os.Args
	vlans := []int{}
	if len(argsWithProg) == 1 {
		Print_usage()
	} else {
		args := argsWithProg[1:]

		for _, arg := range args {
			vlan, err := strconv.Atoi(arg)
			if err != nil {
				fmt.Println("ungültige Eingabe. Als Parameter sind nur VLAN-IDs zugelassen.")
				break
			}
			if vlan < 1 || vlan > 4095 {
				fmt.Printf("%d ist keine gültige VLAN-ID")
				break
			}
			vlans = append(vlans, vlan)
		}
		CalcPrios(vlans)
	}
}
